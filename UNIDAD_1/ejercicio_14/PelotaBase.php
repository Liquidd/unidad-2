<?php

class PelotaBase { 
    protected $peso = 0; 
    protected $textura = 'lisa'; 
    protected $color = 'ninguno'; 

    public function rebotar(){ 
        echo "Estoy rebotando"; 
    } 

    public function rodar(){ 
        echo "Estoy rodando"; 
    } 

    public function __toString(){ 
        return "Soy " . get_called_class() . " y peso {$this->peso}, mi textura es {$this->textura}, y mi color {$this->color}"; 
    } 
} 
