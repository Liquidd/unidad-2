<div class="container">
    <button class="btn btn-outline-success" data-toggle="modal" data-target="#nuevo_producto">NUEVO PRODUCTO</button>
    <div class="row">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Modelo</th>
                    <th>MARCA</th>
                    <th>PRECIO</th>
                    <th>CATEGORIAS</th>
                    <th>SUBCATEGORIAS</th>
                    <th>OPCIONES</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($productos as $value){?>
                    <tr>
                        <th><?php echo $value['modelo'];?></th>
                        <td><?php echo $value['marca'];?></td>
                        <td><?php echo $value['precio'];?></td>
                        <td><?php echo $value['categoria'];?></td>
                        <td><?php echo $value['subcategoria'];?></td>
                        <td>
                            <button type="button" class="btn btn-outline-info vista_productos" data-toggle="modal" data-id='<?php echo $value['id_producto'];?>' data-target="#modal"><i class="fas fa-sync"></i></button>
                            <?php if ($value["estado"] <= 0) {?>
                                <button type="button" class="btn btn-outline-success btn_activar" data-id='<?php echo $value['id_producto'];?>'>
                                    <i class="fas fa-clipboard-check"></i>
                                </button>
                            <?php } else{?>
                                <button type="button" class="btn btn-outline-danger btn_eliminar" data-id='<?php echo $value['id_producto'];?>'>
                                    <i class="fas fa-trash"></i>
                                </button>
                            <?php }?>
                        </td>
                    </tr>
                <?php }?>
            </tbody>
        </table>
    </div>
</div>
<!-- Modal ACTUALIZAR -->
<div class="modal fade bd-example-modal-xl" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"> NOMBRE DEL PRODUDCTO </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
            <div class="form-row">
                <div class="col-md-4 mb-3">
                <label for="producto_nombre">Nombre del Producto</label>
                <input type="text" class="form-control" id="modelo_actualizar" placeholder="Ingresa el Nombre">
                </div>
                <div class="col-md-4 mb-3">
                    <label for="marca_producto">Marca del Producto</label>
                    <input type="text" class="form-control" id="marca_actualizar" placeholder="Ingresa la Marca">
                </div>
                <div class="col-md-4 mb-3">
                    <label for="precio_producto">Precio del Producto</label>
                    <input type="number" class="form-control" id="precio_actualizar" placeholder="$0.00">
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <select class="custom-select" id="id_categorias_actualizar">
                        <option selected>Selecciona Categria</option>
                        <?php foreach ($categoria as $value) {?>
                        <option value="<?php echo $value['id_categoria'];?>"><?php echo $value['nombre'];?></option>
                        <?php }?>
                    </select>
                </div>
                <div class="col-md-6 mb-3">
                    <select class="custom-select" id="id_subcategoria_actualizar">
                        <option selected>Selecciona Subcategoria</option>
                        
                    </select>
                </div>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">CERRAR</button>
        <button type="button" class="btn btn btn-primary btn_editar">ACTUALIZAR</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal NUEVO PRODUCTO-->
<div class="modal fade" id="nuevo_producto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"> NUEVO PRODUDCTO </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form>
            <div class="form-row">
                <div class="col-md-4 mb-3">
                <label for="producto_nombre">Nombre del Producto</label>
                <input type="text" class="form-control" id="modelo" placeholder="Ingresa el Nombre">
                </div>
                <div class="col-md-4 mb-3">
                    <label for="marca_producto">Marca del Producto</label>
                    <input type="text" class="form-control" id="marca" placeholder="Ingresa la Marca">
                </div>
                <div class="col-md-4 mb-3">
                    <label for="precio_producto">Precio del Producto</label>
                    <input type="number" class="form-control" id="precio" placeholder="$0.00">
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-6 mb-3">
                <select class="custom-select" id="id_categorias">
                    <option selected>Selecciona Categria</option>
                    <?php foreach ($categoria as $value) {?>
                       <option value="<?php echo $value['id_categoria'];?>"><?php echo $value['nombre'];?></option>
                    <?php }?>
                </select>
                </div>
                <div class="col-md-6 mb-3">
                <select class="custom-select" id="id_subcategorias">
                    <option selected>Selecciona Subcategoria</option>
                    
                </select>
                </div>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">CERRAR</button>
        <button type="button" class="btn btn btn-success" onClick="alta_producto()">GUARDAR</button>
      </div>
    </div>
  </div>
</div>


<script>

    $(document).ready(function(){
        $("#id_categorias_actualizar").change(function(){            
            $.post(base_url+"productos/lista_subcategoria",{
                id_categoria : $(this).val()
            },function(subcategorias){
                console.log(subcategorias);
                var subcategorias = JSON.parse(subcategorias);
                var opciones_subcategoria = "";
                for (let index = 0; index < subcategorias.length; index++) {
                    opciones_subcategoria += "<option value="+subcategorias[index].id_subcategoria+">"+subcategorias[index].nombre+"</option>";

            }
                $("#id_subcategoria_actualizar").html('<option value="0">Seleccione una Subcategoria </option>'+opciones_subcategoria);
            });
        });
        $("#id_categorias").change(function(){            
            $.post(base_url+"productos/lista_subcategoria",{
                id_categoria : $(this).val()
            },function(subcategorias){
                console.log(subcategorias);
                var subcategorias = JSON.parse(subcategorias);
                var opciones_subcategoria = "";
                for (let index = 0; index < subcategorias.length; index++) {
                    opciones_subcategoria += "<option value="+subcategorias[index].id_subcategoria+">"+subcategorias[index].nombre+"</option>";

            }
                $("#id_subcategorias").html('<option value="0">Seleccione una Subcategoria </option>'+opciones_subcategoria);
            });
        });
        // TODO PENDEJO :v 
        $(".btn_eliminar").on("click", function(){
            var id = $(this).attr("data-id");
            console.log("ID SELECCIONADO: "+id);
            $.post(base_url+"productos/desactivar_producto",{
                id : id
            },function(respuesta){
                let datos = JSON.parse(respuesta);
                console.log("RESULTADO ES "+respuesta);
                console.log(datos);
                swal({title: "Producto Eliminado",icon: "error",}).then((value) => {
                    if(value)location.reload();
                });
            });
        });
        $(".btn_activar").on("click", function(){
            var id = $(this).attr("data-id");
            console.log("ID SELECCIONADO: "+id);
            $.post(base_url+"productos/activar_producto",{
                id : id
            },function(respuesta){
                let datos = JSON.parse(respuesta);
                console.log("RESULTADO ES "+respuesta);
                console.log(datos);
                swal({title: "Producto Activado",icon: "success",}).then((value) => {
                    if(value)location.reload();
                });
            });
        });
        $(".vista_productos").on("click", function(){
            var id = $(this).attr('data-id');
            console.log(id);

            $.post(base_url+"productos/detalle_producto",{
                    id : id
            },function(respuesta){
                let datos = JSON.parse(respuesta);
                console.log(datos);
                $("id_categorias_actualizar").val(datos.id_categoria);
                $("id_subcategoria_actualizar").val(datos.subcategoria);
                $("#modelo_actualizar").val(datos.modelo);
                $("#marca_actualizar").val(datos.marca);
                $("#precio_actualizar").val(datos.precio);
                $(".btn_editar").attr('data-id',id);
            });
        });
        $(".btn_editar").on("click", function(){
            var id = $(this).attr('data-id');
            let data = {
                "modelo" : $('#modelo_actualizar').val(),
                "marca" : $('#marca_actualizar').val(),
                "id_categoria" : $('#id_categorias_actualizar').val(),
                "id_subcategoria" : $('#id_subcategoria_actualizar').val(),
                "precio" : $('#precio_actualizar').val(),
            }
            console.log(data);
            console.log(id);
            $.post(base_url+"productos/actualizar_producto",{
                data : data,
                id : id
            },function(respuesta){
                let datos = JSON.parse(respuesta);
                console.log("RESULTADO ES "+respuesta);
                console.log(datos);
                swal({title: "Producto Actualizado",icon: "info",}).then((value) => {
                    if(value)location.reload();
                });
            });
        });
    });

    function alta_producto() {

        let datos = {
        "modelo" : $('#modelo').val(),
        "marca" : $('#marca').val(),
        "id_categoria" : $('#id_categorias').val(),
        "id_subcategoria" : $('#id_subcategorias').val(),
        "precio" : $('#precio').val(),
        }
        console.log(datos);
        $.post(base_url+"productos/alta_producto",{
        datos : datos
        },function(respuesta){
            console.log(respuesta);
            if(respuesta == 1){
                swal({
                    title: "Nuevo Producto Registrado",
                    icon: "success",
                    button: "ACEPTAR",
                }).then((value) => {
                    location.reload();
                });
            }
            else{
                swal({
                    title: "Nuevo Producto Registrado",
                    icon: "success",
                    button: "ACEPTAR",
                }).then((value) => {
                    location.reload();
                });
            }
        });
    }
</script>